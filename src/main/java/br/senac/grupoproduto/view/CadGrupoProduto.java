/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.grupoproduto.view;

import br.senac.grupoproduto.model.GrupoProduto;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 *
 * @author Aluno
 */
public class CadGrupoProduto extends javax.swing.JDialog {

    private JTextField txDescricao;
    private GrupoProduto grupoProduto;

    public static void main(String[] args) {
        CadGrupoProduto cadGrupoProduto = null;
        cadGrupoProduto = new CadGrupoProduto(null);
        cadGrupoProduto.setVisible(true);

    }

    public CadGrupoProduto(Integer idGrupoProduto) {
        super((JFrame) null, true);
        configurarTela();
        configurarPanelBotoes();
        configurarPanelTipoProduto();
        configirarPanelDados();

        pnBotoes.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        txDescricao.requestFocus();
        if (idGrupoProduto == null) { //inclusão
            grupoProduto = new GrupoProduto();
            txDescricao.setText("<Novo>");
            return;
        }

        /*Data Binding para edição de uma registro
        grupoProduto.GrupoProdutoDAO.getPorId(idGrupoProduto);
        txDescricao.setText(grupoProduto.getIdGrupoProduto().toString());
        txDescricao.setText(grupoProduto.getNomeGrupoProduto());
        if (grupoProduto.getTipoProduto() == TipoProduto.SERVICO) {
            rbServico.setSelected(true);
        } else if (grupoProduto.getTipoProduto() == TipoProduto.MATERIA_PRIMA) {
            rbMateriaPrima.setSelected(true);
        } else if (grupoProduto.getTipoProduto() == TipoProduto.MERCADORIA) {
            rbMercadoria.setSelected(true);
        }*/

    }

    private JPanel pnBotoes;
    private JPanel pnTipoProduto;
    private JPanel pnDados;

    private void configurarTela() {
        //Tela
        setTitle("Cadastrar Grupo Produto");
        setSize(600, 200); //Larg em pixels, Alt em pixels
        setVisible(true); //T - mostra, F - esconde
        setLocationRelativeTo(null); //Centraliza na tela
        setLayout(new BorderLayout());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); //Fechar Janela e excluir o processo     

    }

    private void configurarPanelBotoes() {
        FlowLayout flow = new FlowLayout(FlowLayout.RIGHT);
        pnBotoes = new JPanel(flow);

        //Botão Gravar
        JButton btGravar = new JButton("Gravar");
        btGravar.setSize(new Dimension(80, 30));
        btGravar.setLocation(260, 120);

        //Botão Cancelar
        JButton btCancelar = new JButton("Cancelar");
        btCancelar.setSize(new Dimension(80, 30));
        btCancelar.setLocation(260, 160);

        pnBotoes.add(btGravar);
        pnBotoes.add(btCancelar);

        add(pnBotoes, BorderLayout.PAGE_END);
    }

    private void configurarPanelTipoProduto() {
        FlowLayout flow = new FlowLayout();
        pnTipoProduto = new JPanel(flow);

        //Botões Tipo
        JRadioButton btServico = new JRadioButton("Serviço");
        JRadioButton btMercadoria = new JRadioButton("Mercadoria");
        JRadioButton btMateria_Prima = new JRadioButton("Matéria Prima");

        ButtonGroup group = new ButtonGroup();
        group.add(btServico);
        group.add(btMercadoria);
        group.add(btMateria_Prima);

        pnTipoProduto.add(btServico);
        pnTipoProduto.add(btMercadoria);
        pnTipoProduto.add(btMateria_Prima);

    }

    private void configirarPanelDados() {
        GridLayout grid = new GridLayout(3, 2);
        pnDados = new JPanel(grid);

        //Label Cógido
        JLabel lbCodigo = new JLabel("Código:");
        JTextField txCodigo = new JTextField();
        lbCodigo.setHorizontalAlignment(SwingConstants.RIGHT);

        //Label Descrição
        JLabel lbDescricao = new JLabel("Descricao:");
        txDescricao = new JTextField();
        lbDescricao.setHorizontalAlignment(SwingConstants.RIGHT);

        //Label Cógido
        JLabel lbTipo = new JLabel("Tipo:");
        lbTipo.setHorizontalAlignment(SwingConstants.RIGHT);

        pnDados.add(lbCodigo);
        pnDados.add(txCodigo);
        pnDados.add(lbDescricao);
        pnDados.add(txDescricao);
        pnDados.add(lbTipo);
        pnDados.add(pnTipoProduto);

        add(pnDados, BorderLayout.CENTER);

    }

}
