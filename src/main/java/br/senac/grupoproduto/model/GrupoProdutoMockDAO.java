package br.senac.grupoproduto.model;

import br.senac.dd.componente.model.BaseDAO;
import br.senac.dd.componente.model.ParametrosInvalidosException;
import br.senac.produto.model.TipoProduto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class GrupoProdutoMockDAO implements BaseDAO<GrupoProduto, Integer> {

    private static ArrayList<GrupoProduto> listaGrupoProduto = new ArrayList<>();

    public GrupoProdutoMockDAO() { //construtor
        if (listaGrupoProduto.size() > 0) {
            return;
        }
        /*/MOCK: Objetos para testes
        listaGrupoProduto.add(new GrupoProduto(1, "Alimentos", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(2, "Estética", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(3, "Higiene", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(4, "Limpeza", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(5, "Cobre", TipoProduto.MATERIA_PRIMA));
        listaGrupoProduto.add(new GrupoProduto(6, "Mecânica", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(7, "Segurança", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(8, "Educação", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(9, "Automóveis", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(10, "Lã", TipoProduto.MATERIA_PRIMA));
        listaGrupoProduto.add(new GrupoProduto(11, "Algodão", TipoProduto.MATERIA_PRIMA)); */
    }
    
    @Override
    public Integer inserir(GrupoProduto object) {

        if (object == null) {
            throw new ParametrosInvalidosException(1, "Grupo produto inválido!");
        } else if (object.getNomeGrupoProduto() == null
                || object.getNomeGrupoProduto().equals("")) {
            throw new ParametrosInvalidosException(2, "Nome do grupo produto inválido!");
        }

        int maiorId = 0;
        for (GrupoProduto grupoProd : listaGrupoProduto) {
            if (grupoProd.getIdGrupoProduto() > maiorId) {
                maiorId = grupoProd.getIdGrupoProduto();
            }
        }
        maiorId = maiorId + 1;
        object.setIdGrupoProduto(maiorId);
        listaGrupoProduto.add(object);
        return maiorId;
    }
    
    @Override
    public boolean alterar(GrupoProduto objeto) {
        int index = 0;
        for(GrupoProduto grupProd : listaGrupoProduto){
            System.out.println("index: "+ index);
            if(objeto.equals(grupProd) == true){
                listaGrupoProduto.set(index, objeto);
                System.out.println("grupProd: "+ grupProd.getNomeGrupoProduto());
                return true;
            }
            index++;
        }
        return false;
    }
    
    @Override
    public boolean excluir(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public GrupoProduto getPorId(Integer id) {
        for(GrupoProduto grupProd : listaGrupoProduto){
            if(grupProd.getIdGrupoProduto().equals(id)){
                return grupProd;
            }
        }
        return null;
    }
    
    public List<GrupoProduto> listarPorNome(String nome){
        List<GrupoProduto> listaNome = new LinkedList<>();
         
        for(GrupoProduto grupProd : listaGrupoProduto){
            if(nome != null &&
               grupProd.getNomeGrupoProduto().toUpperCase().contains(nome.toUpperCase())){
                listaNome.add(grupProd);
            }
        }
        Collections.sort(listaNome, 
            new Comparator<GrupoProduto>() {
               @Override
               public int compare(GrupoProduto gp1, GrupoProduto gp2) {
                   int comp = gp1.getNomeGrupoProduto().
                           compareToIgnoreCase(gp2.getNomeGrupoProduto());
                   if(comp == 0){ //Nomes iguais
                       return gp1.getIdGrupoProduto().compareTo(gp2.getIdGrupoProduto());
                   }else {
                       return comp;
                   }
                   
               }
            } 
        );
        return listaNome;
    }
    
    public HashSet<GrupoProduto> listarTodos(){
        HashSet<GrupoProduto> set = new HashSet<>();
        
        set.addAll(listaGrupoProduto);
        for(GrupoProduto gp : set){
            System.out.println(gp.getNomeGrupoProduto());
        }
        return set;
    }
    
    public HashMap<TipoProduto, List<GrupoProduto>> getMapPorTipoProduto(){
        HashMap<TipoProduto, List<GrupoProduto>> map = new HashMap<>();
        map.put(TipoProduto.SERVICO, new ArrayList<GrupoProduto>());
        map.put(TipoProduto.MATERIA_PRIMA, new ArrayList<GrupoProduto>());
        map.put(TipoProduto.MERCADORIA, new ArrayList<GrupoProduto>());
        //implementar lógica
        return map;
    }
    
    
        
}
