package br.senac.grupoproduto.model;

import br.senac.produto.model.TipoProduto;
import java.util.Date;
import java.util.Objects;

public class GrupoProduto {
    
    private Integer idGrupoProduto;
    private String nomeGrupoProduto;
    private TipoProduto tipoProduto;
    private Date dataInclusao;
    private Date dataExclusao;
    private Float percDesconto;

    public GrupoProduto() {
    }

    public GrupoProduto(Integer idGrupoProduto, String nomeGrupoProduto, TipoProduto tipoProduto, Date dataInclusao, Date dataExclusao, Float percDesconto) {
        this.idGrupoProduto = idGrupoProduto;
        this.nomeGrupoProduto = nomeGrupoProduto;
        this.tipoProduto = tipoProduto;
        this.dataInclusao = dataInclusao;
        this.dataExclusao = dataExclusao;
        this.percDesconto = percDesconto;
    }
    
    public Integer getIdGrupoProduto() {
        return idGrupoProduto;
    }

    public void setIdGrupoProduto(Integer idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
    }

    public String getNomeGrupoProduto() {
        return nomeGrupoProduto;
    }

    public void setNomeGrupoProduto(String nomeGrupoProduto) {
        this.nomeGrupoProduto = nomeGrupoProduto;
    }

    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public Float getPercDesconto() {
        return percDesconto;
    }

    public void setPercDesconto(Float percDesconto) {
        this.percDesconto = percDesconto;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoProduto other = (GrupoProduto) obj;
        if (!Objects.equals(this.idGrupoProduto, other.idGrupoProduto)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nomeGrupoProduto;
    }
    
    
    
    
}
