/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.grupoproduto.model;


import br.senac.componentes.db.ConexaoDB;
import br.senac.dd.componente.model.BaseDAO;
import br.senac.produto.model.TipoProduto;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GrupoProdutoDAO  implements BaseDAO<GrupoProduto, Integer> {
    public GrupoProdutoDAO() {
    
    }
        
    public Integer inserir(GrupoProduto grupoProduto) throws SQLException {
        Integer pk = 0;
        String sqlInsert = "INSERT INTO GRUPOPRODUTO "+
                "(NOMEGRUPOPRODUTO, TIPO, DATAINCLUSAO, PERCDESCONTO) " +
                "VALUES (";
        sqlInsert += "'" + grupoProduto.getNomeGrupoProduto() + "', ";
        if (grupoProduto.getTipoProduto() == null)
            throw new RuntimeException("Tipo grupo não pode ser nulo.");
        else
            sqlInsert += grupoProduto.getTipoProduto().getId()+",";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sqlInsert += "{ts'" + sdf.format (grupoProduto.getDataInclusao()) + "'}, ";
        sqlInsert += grupoProduto.getPercDesconto() + ")";
        System.out.print(sqlInsert);
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regCriados = stm.executeUpdate(sqlInsert, Statement.RETURN_GENERATED_KEYS);
        ResultSet rsPK = stm.getGeneratedKeys();
        if(rsPK.next()){
            pk = rsPK.getInt(1);
            return pk;
        }
        throw new RuntimeException("Erro inesperado ao incluir grupo produto!");
    }

    public boolean alterar(GrupoProduto grupoProduto) throws SQLException {
        String sqlAlterar = "UPDATE grupoproduto SET ";
        sqlAlterar += " nomegrupoproduto = '"+ grupoProduto.getNomeGrupoProduto() +"', \n"; 
        sqlAlterar += "	tipo = "+ grupoProduto.getTipoProduto().getId() +", \n"; 
        sqlAlterar += " percdesconto = "+grupoProduto.getPercDesconto()+" \n";
        sqlAlterar += "WHERE idgrupoproduto = " + grupoProduto.getIdGrupoProduto();
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regAlterados = stm.executeUpdate(sqlAlterar);
        return (regAlterados == 1);
    }

    public boolean excluir(Integer id) throws SQLException {
        String sqlExclusao = "DELETE FROM grupoproduto WHERE idgrupoproduto = " + id;
        Connection conn = ConexaoDB.getInstance().getConnection();
        try{
            Statement stm = conn.createStatement();
            int regAlterados = stm.executeUpdate(sqlExclusao);
            return (regAlterados == 1);
        }catch(SQLException e){
            String sqlAtualizaData = "UPDATE grupoproduto set dataexclusao = CURDATE() "+
                                     "WHERE idgrupoproduto = " + id;
            Statement stm = conn.createStatement();
            int regAlterados = stm.executeUpdate(sqlAtualizaData);
            return (regAlterados == 1);
        }
    }
    
    public GrupoProduto getPorId(Integer id) {
        try{
            Connection conn = ConexaoDB.getInstance().getConnection();
            Statement stm = conn.createStatement();
            String sql = "SELECT * FROM grupoproduto "
                        + "WHERE idgrupoproduto = " + id;
            ResultSet rs = stm.executeQuery(sql);
            if(rs.next() == false){
                throw new RuntimeException("Registro não encontrado!");
            }
            GrupoProduto gp = new GrupoProduto();
            gp.setNomeGrupoProduto(rs.getString("nomeGrupoProduto"));
            //continua databinding
            
            return gp;
        } catch(SQLException ex){
            throw new RuntimeException("Ocorreu um erro inesperado ao consultar daddos"
                    + "em grupo produto, consulte o suporte:"+ex.getMessage(), ex);
        }

    }
    
    private GrupoProduto getGrupoProduto(ResultSet rs) throws SQLException{
        GrupoProduto gp = new GrupoProduto();
        gp.setNomeGrupoProduto(rs.getString("nomeGrupoProduto"));
        gp.setDataExclusao(rs.getDate("dataexclusao")); //
        gp.setDataInclusao(rs.getTimestamp("datainclusao"));
         switch (rs.getInt("tipo")) {
             case 1:
                 gp.setTipoProduto(TipoProduto.MERCADORIA);
                 break;
             case 2:
                 gp.setTipoProduto(TipoProduto.SERVICO);
                 break;
             case 3:
                 gp.setTipoProduto(TipoProduto.MATERIA_PRIMA);
                 break;
         }
        return gp;
    }


    public List<GrupoProduto> listarGrupoProdutoPorNome(String nome) throws SQLException {
        if(nome==null && nome.trim().equals("")){
            return listarTodos();
        }
        List<GrupoProduto> lista = new ArrayList<>();
        String sql = "SELECT *FROM  grupoProduto\n" 
                +" WHERE UPPER(nomegrupoproduto) LIKE UPPER ('%" +nome+ "%')";
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        GrupoProduto grupProd;
        while (rs.next()){
            grupProd = getGrupoProduto(rs);
            lista.add(grupProd);
        }
        return lista;
    }

    public List<GrupoProduto> ordenarPorNome(boolean crescente) throws SQLException {
        List<GrupoProduto> lista = listarTodos();
        String sql = "SELECT * FROM GRUPOPRODUTO "+
                "ORDER BY nomeGrupoProduto ";
        if(crescente)
            sql += "ASC";
        else
            sql += "DESC";
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        GrupoProduto grupProd;
        while (rs.next()){
            grupProd = getGrupoProduto(rs);
            lista.add(grupProd);
        }
        return lista;
    }

    /**
     * converta o ArrayList para um LinkedList retorne a lista ordenada por idGrupoProduto, 
     * utilize Comparator e Collections.sort;
     * @return 
     */
    public List<GrupoProduto> listarTodos() throws SQLException {
        LinkedList<GrupoProduto> listaTodos = new LinkedList<>(ordenarPorNome(true));
        return listaTodos;
    }

    /**
     * Use um HashMap e separe a lista de Grupos de Produtos em duas, 
     * uma contendo apenas o TipoProduto.PRODUTO e outra TipoProduto.SERVICO. 
     * Depois imprimiva os chaves e na sequência imprima os valores.
     * @return 
     */
    public HashMap<TipoProduto, List<GrupoProduto>> obterMapId() {
        HashMap<TipoProduto, List<GrupoProduto>> hashMap = new HashMap<>();
        hashMap.put(TipoProduto.SERVICO, new ArrayList<>());
        hashMap.put(TipoProduto.MATERIA_PRIMA, new ArrayList<>());
        hashMap.put(TipoProduto.MERCADORIA, new ArrayList<>());
        
        //Implemente aqui
        
        for (List<GrupoProduto> listaGrupProd : hashMap.values()) {
            for (GrupoProduto grupProd : listaGrupProd) {
                System.out.println(grupProd);    
            }
        }
        
        for (TipoProduto tipo : hashMap.keySet()) {
            System.out.println("Tipo: " + tipo);
        }
        
        return hashMap;
    }

}