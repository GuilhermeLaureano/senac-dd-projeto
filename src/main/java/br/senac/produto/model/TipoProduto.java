package br.senac.produto.model;

public enum TipoProduto {

    MERCADORIA(1, "Mercadoria"), 
    MATERIA_PRIMA(2, "Matéria Prima"), 
    SERVICO(3, "Serviço");
    
    
    private final Integer id;
    private final String nome;

    private TipoProduto(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }
    
    public String getNome() {
        return nome;
    }
    
    public static TipoProduto getMERCADORIA() {
        return MERCADORIA;
    }

    public static TipoProduto getMATERIA_PRIMA() {
        return MATERIA_PRIMA;
    }

    public static TipoProduto getSERVICO() {
        return SERVICO;
    }

}
