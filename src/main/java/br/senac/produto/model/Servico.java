package br.senac.produto.model;

import br.senac.grupoproduto.model.GrupoProduto;
import java.util.Date;

public class Servico extends Produto {
    
    private Long idServico;
    private Float percISS;
    
    public Servico(Long idServico) {
        super(idServico);
        this.idServico = idServico;
    }

    public Servico() {
    }
    
    public Servico(Long idProduto, String nomeProduto, TipoProduto tipoProduto, Date dataCriacao, Float percICMS, GrupoProduto grupoProduto,Float percISS) {
        super(idProduto, nomeProduto, tipoProduto, dataCriacao, percICMS, grupoProduto);
        this.idServico = idProduto;
        this.percISS = percISS;
    }

    public Long getIdServico() {
        return idServico;
    }

    public void setIdServico(Long idServico) {
        this.idServico = idServico;
    }

    public Float getPercISS() {
        return percISS;
    }

    public void setPercISS(Float percISS) {
        this.percISS = percISS;
    }

    @Override
    public Float getTotalPercImposto() {
        return (percISS == null ? 0F : percISS) + 
                (super.getPercICMS() == null ? 0F : super.getPercICMS());
    }
    
    
}
