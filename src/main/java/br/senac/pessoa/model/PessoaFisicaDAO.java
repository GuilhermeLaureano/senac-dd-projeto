/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.pessoa.model;

import br.senac.dd.componente.model.BaseDAO;
import br.senac.componentes.db.ConexaoDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class PessoaFisicaDAO implements BaseDAO<PessoaFisica, Long>{
    
    public static void main(String[] args) throws SQLException {
        PessoaFisicaDAO dao = new PessoaFisicaDAO();
        PessoaFisica pesFis = dao.getPorId(646L);
        System.out.println(pesFis);
        System.out.println(dao.getPorNome("Mar"));
    }
    
    @Override
    public PessoaFisica getPorId(Long id) throws SQLException {
        String sql = "SELECT * FROM pessoa pes " +
                    "INNER JOIN pessoafisica pf ON pf.idpessoa = pes.idpessoa " +
                    "LEFT JOIN tipodeficiencia tdp ON \n" +
                    "WHERE pf.idpessoa = ? ";
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement pStm = conn.prepareStatement(sql);
        pStm.setLong(1, id);
        ResultSet rs = pStm.executeQuery();
        if (rs.next() == false){
            return null;
        }
        return getPessoaFisica(rs);
    }
    
    private PessoaFisica getPessoaFisica(ResultSet rsPesFis) throws SQLException {
        PessoaFisica pFisica = new PessoaFisica();
        bindPessoa(rsPesFis, pFisica);
        pFisica.setCpf(rsPesFis.getString("cpf"));
        pFisica.setDtNascimento(rsPesFis.getDate("dtNascimento"));
        pFisica.setPcd(rsPesFis.getBoolean("pcd"));
        pFisica.setRenda(rsPesFis.getDouble("renda"));
        if(rsPesFis.getString("sexo").equals("M")){
            pFisica.setSexo(Sexo.MASCULINO);
        }else if(rsPesFis.getString("sexo").equals("F")){
            pFisica.setSexo(Sexo.FEMININO);
        }if(rsPesFis.getObject("idtipodeficiencia") != null){
            TipoDeficiencia tdDef = new TipoDeficiencia(rsPesFis.getInt("idtipodeficiencia"), rsPesFis.getString("nome"));
            pFisica.setTipoDeficiencia(tdDef);
        }
        return pFisica;
    }
    
    private void bindPessoa(ResultSet rs, Pessoa pessoa) throws SQLException {
        pessoa.setDtCadastro(rs.getTimestamp("dtCadastro"));
        pessoa.setEmail(rs.getString("email"));
        pessoa.setId(rs.getLong("idpessoa"));
        pessoa.setNome(rs.getString("nomepessoa"));
        pessoa.setTelefone(rs.getString("telefone"));
        pessoa.setTipo(rs.getString("tipo"));
        
    }
    
    @Override
    public boolean excluir(Long id) throws SQLException {
        String sqlPesExcluir = "DELETE FROM pessoa WHERE idpessoa = ? ";
        String sqlPesFisExcluir = "DELETE FROM pessoafisica WHERE idpessoa = ? ";
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement pst = conn.prepareStatement(sqlPesFisExcluir);
        pst.setLong(1, id);
        int regExcluidos = pst.executeUpdate();
        if(regExcluidos == 0){
            return false;
        }
        pst = conn.prepareStatement(sqlPesExcluir);
        pst.setLong(1, id);
        regExcluidos = pst.executeUpdate();
        if(regExcluidos == 0){
            return false;
        }
        return true;
    }

    
    @Override
    public boolean alterar(PessoaFisica pf) throws SQLException {
        String sqlAlterar = "UPDATE pessoa SET ";
        sqlAlterar += " nomePessoa = '"+ pf.getNome() +"', \n"; 
        sqlAlterar += "	tipo = "+ pf.getTipo() +", \n"; 
        sqlAlterar += " telefone = "+pf.getTelefone()+" \n";
        sqlAlterar += " email = "+ pf.getEmail()+" \n";
        sqlAlterar += "WHERE idPessoa = " + pf.getId();
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regAlterados = stm.executeUpdate(sqlAlterar);
        return (regAlterados == 1);
    }

    @Override
    public Long inserir(PessoaFisica pessoaFis) throws SQLException {
        
        String sqlPessoa = "INSERT INTO pessoa\n" +
                            "(nomePessoa, tipo, telefone, email,\n" +
                            "idEndereco,dtcadastro) VALUES\n" +
                            "(?, ?, ?, ?, ?, ?)";
        Connection conn = ConexaoDB.getInstance().getConnection();
        try{ 
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement(sqlPessoa,
                                  PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pessoaFis.getNome());
            ps.setString(2, "F");
            ps.setObject(3, pessoaFis.getTelefone());
            ps.setObject(4, pessoaFis.getEmail());
            ps.setObject(5, null);
            ps.setTimestamp(6, new java.sql.Timestamp(System.currentTimeMillis()));
            
            ps.executeUpdate();
            
            ResultSet rsChave = ps.getGeneratedKeys();
            Long idChave;
            if(rsChave.next()){
                idChave = rsChave.getLong(1);
            }else {
                throw new RuntimeException("Erro ao tentar inserir pessoa.");
            }
            
            //continue aqui fazendo a inserção da pessoafisica
            
            conn.commit();
            return idChave;
        }catch(Exception e){
            conn.rollback();
            throw new RuntimeException("Erro ao inserir pessoa física: " + e.getMessage(), e);
        }
        
    }
	
    /**
    * Se o nome for nulo ou tiver menos de três caracteres será gerada uma exceção para evitar o retorno de muitos registros.
    */
    public List<PessoaFisica> getPorNome(String nome) throws SQLException {
        if (nome == null || nome.length() <= 2){
		throw new RuntimeException("Erro, nome informado é inválido.");
	}
	String sql = "SELECT * FROM pessoa pes "+
		"inner join pessoafisica pf on pf.idpessoa = pes.idpessoa "+
                "LEFT JOIN tipodedeficiencia tdp on \n"+
                "tdp.idtipodeficiencia = pf.idtipodeficiencia \n"+
		"where pes.nomePessoa like ? ";
	Connection conn = ConexaoDB.getInstance().getConnection();
	PreparedStatement pst = conn.prepareStatement(sql);
	pst.setString(1, "%" + nome + "%");
	ResultSet rs = pst.executeQuery();
	ArrayList<PessoaFisica> listaPesFis = new ArrayList<>();
	while(rs.next()){
		listaPesFis.add(getPessoaFisica(rs));
	}
	return listaPesFis;
    }




    
    
    
}
