/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.pedidovenda.model;

/**
 *
 * @author guila
 */
public class ItemPedidoVenda {
    
    private Long idItemPedido;
    private int quantidade;
    private Long valor;
    private Long valorTotal;
    private Long idProduto;
    private Long idPedido;

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

    public Long getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Long valorTotal) {
        this.valorTotal = valorTotal;
    }
        
    public Long getIdItemPedido() {
        return idItemPedido;
    }
    
    public void setIdItemPedido (Long idItemPedido) {
        this.idItemPedido = idItemPedido;
    }

    public Long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Long idProduto) {
        this.idProduto = idProduto;
    }

    public Long getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Long idPedido) {
        this.idPedido = idPedido;
    }
    
    
}
