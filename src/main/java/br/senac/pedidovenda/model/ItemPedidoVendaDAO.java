/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.pedidovenda.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import br.senac.componentes.db.ConexaoDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import br.senac.dd.componente.model.BaseDAO;
import java.sql.Statement;

/**
 *
 * @author guila
 */
public class ItemPedidoVendaDAO implements BaseDAO<ItemPedidoVenda, Long> {

    /* Utilize Statement ou PreparedStatement */

    @Override
    public ItemPedidoVenda getPorId(Long idItemPedido) throws SQLException {
//        throw new UnsupportedOperationException("Ainda não implementado.");

        String sql = "select * from itempedido "
                + "where idItemPedido =" + idItemPedido;
        Connection conn = ConexaoDB.getInstance().getConnection();
        ResultSet rs = conn.createStatement().executeQuery(sql);
        if (rs.next() == false) {
            throw new RuntimeException("IdItemPedido não encontrado: " + idItemPedido + ".");
        }
        ItemPedidoVenda itemPedidoVenda = getItemPedidoVenda(rs);
        return itemPedidoVenda;

    }

    /**
     * Utilize PreparedStatement
     *
     * @throws java.sql.SQLException
     */
    @Override
    public Long inserir(ItemPedidoVenda item) throws SQLException {
        String sql = "insert into itempedido (quantidade, valor, "
                + "valortotal, idproduto, idpedido) "
                + "values (?,?,?,?,?)";
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement pstm = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        pstm.setInt(1, item.getQuantidade());
        pstm.setLong(2, item.getValor());
        pstm.setLong(3, item.getValorTotal());
        pstm.setLong(4, item.getIdProduto());
        pstm.setLong(5, item.getIdPedido());
        pstm.executeUpdate();
        ResultSet rsPK = pstm.getGeneratedKeys();
        if (rsPK.next()) {
            return rsPK.getLong(1); //Retorma a chave primária gerada pelo BD
        }
        throw new SQLException("Erro inesperado, item não incluído no pedido");
    }

    /* Utilize Statement ou PreparedStatement*/
    @Override
    public boolean excluir(Long idItemPedido) throws SQLException {
        throw new UnsupportedOperationException("Ainda não implementado.");
    }

    /* Utilize Statement */
    @Override
    public boolean alterar(ItemPedidoVenda item) throws SQLException {
        if (item == null || item.getIdItemPedido() == null) {
            throw new RuntimeException("Item com idItemPedido igual a nulo.");
        }

        String sql = "UPDATE `projeto`.`itempedido`\n"
                + "SET\n"
                + "`idItemPedido` =" + item.getIdItemPedido() + ",\n"
                + "`quantidade` = " + item.getQuantidade() + ",\n"
                + "`valor` = " + item.getValor() + ",\n"
                + "`valorTotal` = " + item.getValorTotal() + ",\n"
                + "`idproduto` = " + item.getIdProduto() + ",\n"
                + "`idPedido` = " + item.getIdPedido() + "\n"
                + "WHERE `idItemPedido` = " + item.getIdItemPedido();
        System.out.println(sql);
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement ps = conn.createStatement();
        int regAlterados = ps.executeUpdate(sql);
        return (regAlterados == 1);
    }

    public List<ItemPedidoVenda> getItensPorPedido(Long idPedido) throws SQLException {
        String sql = "Select * From itempedido Where idpedido = " + idPedido;

        /*Ao invés de criar uma variável para cada objeto (Statement e Connection), 
        estou encadeando as chamadas de métodos.*/
        ResultSet rs = ConexaoDB.getInstance().getConnection().createStatement().executeQuery(sql);
        List<ItemPedidoVenda> listaItens = new ArrayList<>();
        while (rs.next()) {
            listaItens.add(getItemPedidoVenda(rs));
        }
        return listaItens;
    }

    /* Converter o ResultSet num objeto do tipo ItemPedidoVenda */
    private ItemPedidoVenda getItemPedidoVenda(ResultSet rs) {
        throw new UnsupportedOperationException("Ainda não implementado.");
    }
}
