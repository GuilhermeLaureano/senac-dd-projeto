/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.pedidovenda.model;

/**
 *
 * @author guila
 */
public enum TipoPedidoVenda {
    
    PEDIDO(1), ORCAMENTO(2);
    
    private Integer id;
            
    private TipoPedidoVenda(Integer id){
        this.id = id;
    }
    
    public Integer getId() {
        return id;
    }
    
    public static TipoPedidoVenda getPorId(Integer id){
        if(id != null){
            switch(id){
                case 1:
                    return PEDIDO;
                case 2:
                    return ORCAMENTO;
            }
        }
        return null;
    }
    
}
