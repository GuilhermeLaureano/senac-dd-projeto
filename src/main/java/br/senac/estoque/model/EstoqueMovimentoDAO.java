/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.estoque.model;

import br.senac.componentes.db.ConexaoDB;
import br.senac.dd.componente.model.BaseDAO;
import br.senac.produto.model.Mercadoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class EstoqueMovimentoDAO implements BaseDAO<EstoqueMovimento, Long> {

    /**
     * A partir de um ResultSet passado como parâmetro, cujo cursor está
     * posicionado em alguma linha da tabela estoquemovto, faça o DataBinding
     * dos dados do ResultSet para um objeto do tipo EstoqueMovimento.
     *
     * @param rs
     */
    private EstoqueMovimento getEstoqueMovimento(ResultSet rs) throws SQLException {

        EstoqueMovimento estMov = new EstoqueMovimento();
        Mercadoria mercadoria = new Mercadoria();
        mercadoria.setIdMercadoria(rs.getLong("idMovtoEstoque"));
        estMov.setProduto(mercadoria);
        estMov.setQuantidade(rs.getDouble("quantidade"));
        estMov.setTipoMovto(TipoMovimentoEstoque.getTipoPorCodigo(rs.getString("tipoMovto")));
        estMov.setDataMovto(rs.getDate("dataMovto"));
        estMov.getProduto().setIdProduto(rs.getLong("idProduto"));
        estMov.setIdUsuario(rs.getInt("idUsuario"));
        estMov.setObservacoes("observacoes");

        return estMov;

    }

    /**
     * Utilizando Statement ou PreparedStatement atualize o Saldo de Estoque. Se
     * o produto não existir na tabela estoquesaldo, é necessário incluir um
     * registro na tabela estoquesaldo desse mesmo produto. Se o produto existir
     * na tabela estoquesaldo, é necessário apenas somar a quantidade (se o
     * parâmetro "quantidade" tiver valor positivo, vai aumentar a quantidade no
     * estoque, se for negativo, vai diminuir a quantidade no estoque).
     *
     * @param idMovtoEstoque
     */
    private void atualizarSaldoEstoque(Long idProduto, Double quantidade) throws SQLException {

        String sql = "SELECT `estoquesaldo`.`idProduto`,\n"
                + "    `estoquesaldo`.`saldo`\n"
                + "FROM `projeto`.`estoquesaldo`;";
        Connection conn = ConexaoDB.getInstance().getConnection();
        ResultSet rs = conn.createStatement().executeQuery(sql);
        if (rs == null) {
            String inse = "INSERT INTO `projeto`.`estoquesaldo`\n"
                    + "(`idProduto`,\n"
                    + "`saldo`)\n"
                    + "VALUES\n"
                    + "(" + idProduto + ",\n"
                    + quantidade + ");";
            ResultSet rsins = conn.createStatement().executeQuery(inse);
            
        } else {
            String upd = "UPDATE `projeto`.`estoquesaldo`\n"
                    + "SET\n"
                    + "`idProduto` = " + idProduto + ",\n"
                    + "`saldo` = " + quantidade + "\n"
                    + "WHERE `idProduto` =" + idProduto + ";";
            System.out.println(sql);
            ResultSet rsupd = conn.createStatement().executeQuery(upd);

        }

    }

    /**
     * Utilize PreparedStatement. Após inserir (INSERT) um registro na tabela
     * estoquemovto, atualize também a tabela estoquesaldo através do método
     * atualizarSaldoEstoque. Utilize transação, pois iremos atualizar dois
     * registros de tabelas diferentes, caso ocorra qualquer exceção, faça o
     * rollback. Todos os campos são obrigatórios, com exceção dos campos
     * idUsuario e observacoes que podem ou não ser nulos (null).
     *
     * @param movtoEstoque
     * @return
     * @throws java.sql.SQLException
     */
    @Override
    public Long inserir(EstoqueMovimento movtoEstoque) throws SQLException {

        String sql = "INSERT INTO projeto.estoquemovto (quantidade,tipoMovto,dataMovto,\n"
                + "idProduto,idUsuario,observacoes)\n"
                + "VALUES (?,?,?,?,?,?);";
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement pstm = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        pstm.setDouble(1, movtoEstoque.getQuantidade());
        pstm.setObject(2, movtoEstoque.getTipoMovto());
        pstm.setObject(3, movtoEstoque.getDataMovto());
        pstm.setLong(4, movtoEstoque.getProduto().getIdProduto());
        pstm.setInt(5, movtoEstoque.getIdUsuario());
        pstm.setString(6, movtoEstoque.getObservacoes());
        ResultSet rsPK = pstm.getGeneratedKeys();

        if (rsPK.next()) {
            return rsPK.getLong(1);
        }
        throw new SQLException("Erro inesperado, item não incluído no EstoqueMovimento");

    }

    /**
     * Utilize PreparedStatement. Será necessário buscar a quantidade do
     * movimento que está sendo excluído, depois atualizar o estoquesaldo
     * através do método atualizarSaldoEstoque, por último, faça a exclusão
     * (DELETE) do registro em estoquemovto conforme idMovtoEstoque passado.
     * Transacione essa operação, caso ocorra alguma exceção, faça rollback.
     *
     * @param idMovtoEstoque
     * @return
     * @throws java.sql.SQLException
     */
    @Override
    public boolean excluir(Long idMovtoEstoque) throws SQLException {
        Connection conn = ConexaoDB.getInstance().getConnection();
        try {
            conn.setAutoCommit(false);
            String sql = "delete from `projeto`.`estoquemovto` where idMovtoEstoque = ? ";
            EstoqueMovimento estMov = getPorId(idMovtoEstoque);
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setLong(1, idMovtoEstoque);

            int excluidos = pstm.executeUpdate();
            if (excluidos == 0) {
                return false;
            }
            atualizarSaldoEstoque(estMov.getProduto().getIdProduto(), -1 * estMov.getQuantidade());
            conn.commit();
        } catch (SQLException e) {
            conn.rollback();
            conn.setAutoCommit(true);
            throw new RuntimeException("Erro ao excluir pedido de venda: " + e.getMessage() + ".", e);
        }
        return true;

    }

    /**
     * Utilize Statement. Será necessário buscar a quantidade do movimento que
     * está sendo alterado, caso a quantidade que está no banco de dados for
     * diferente da quantidade passada no atributo movtoEstoque.getQuantidade(),
     * é necessário atualizar a tabela estoquesaldo através do método
     * atualizarSaldoEstoque. Após atualização do saldo, alterar (UPDATE) o
     * registro na tabela estoquemovto. Utilize transação, se qualquer exceção
     * ocorrer, faça o rollback. Todos os campos são obrigatórios, com exceção
     * dos campos idUsuario e observacoes que podem ou não ser nulos (null).
     *
     * @param movtoEstoque
     * @return
     * @throws java.sql.SQLException
     */
    @Override
    public boolean alterar(EstoqueMovimento movtoEstoque) throws SQLException {

        if (movtoEstoque == null || movtoEstoque.getIdMovtoEstoque() == null) {
            throw new RuntimeException("Item com idMovtoEstoque igual a nulo.");
        }

        String sql = "UPDATE projeto.estoquemovto\n"
                + "SET\n"
                + "idMovtoEstoque = " + movtoEstoque.getIdMovtoEstoque() + ",\n"
                + "quantidade` = " + movtoEstoque.getQuantidade() + ",\n"
                + "tipoMovto` = " + movtoEstoque.getTipoMovto() + ",\n"
                + "dataMovto` = " + movtoEstoque.getDataMovto() + ",\n"
                + "idProduto` = " + movtoEstoque.getProduto().getIdProduto() + ",\n"
                + "idUsuario` = " + movtoEstoque.getIdUsuario() + ",\n"
                + "observacoes` = " + movtoEstoque.getObservacoes() + ",\n"
                + "WHERE idMovtoEstoque = " + movtoEstoque.getIdMovtoEstoque();
        System.out.println(sql);
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement ps = conn.createStatement();
        int regAlterados = ps.executeUpdate(sql);
        return (regAlterados == 1);

    }

    /**
     * Utilizando Statement ou PreparedStatement, a partir do idProduto
     * informado e dataInicioMovto (considere como DATE apenas), retorne a lista
     * de objetos do tipo EstoqueMovimento. Utilize a função getEstoqueMovimento
     * para fazer o DataBinding dos dados de cada uma das linhas do ResultSet
     * para o objeto EstoqueMovimento. Retorne apenas os registro cujo dataMovto
     * seja maior ou igual a dataInicioMovto.
     *
     * @param idProduto
     * @param dataInicioMovto
     * @return
     * @throws java.sql.SQLException
     */
    public List<EstoqueMovimento> listarPorProduto(Long idProduto, Date dataInicioMovto) throws SQLException {
        //throw new UnsupportedOperationException("Não implementado ainda!");
        List<EstoqueMovimento> lista = new ArrayList<>();
        String sql = "select * from estoquemovto\n"
                + "where idMovtoEstoque = " + idProduto + ",\n"
                + "and dataMovto >= " + dataInicioMovto + ",\n";
        Connection conn = ConexaoDB.getInstance().getConnection();
        ResultSet rs = conn.createStatement().executeQuery(sql);
        EstoqueMovimento estMov;
        while (rs.next()) {
            estMov = getEstoqueMovimento(rs);
            lista.add(estMov);
        }
        return lista;
    }

    /**
     * A partir do idMovtoEstoque, utilizando Statement ou PreparedStatement,
     * retorne o objeto do tipo EstoqueMovimento. Utilize o método
     * getEstoqueMovimento para fazer o DataBinding.
     *
     * @param id
     * @return
     * @throws java.sql.SQLException
     */
    @Override
    public EstoqueMovimento getPorId(Long id) throws SQLException {

        String sql = "select * from estoquemovto where idMovtoEstoque = " + id;
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        if (rs.next() == false) {
            throw new RuntimeException("Estoque movimento não encontrado: " + id + ".");
        }
        EstoqueMovimento estMov = getEstoqueMovimento(rs);
        return estMov;
    }
}
